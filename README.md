#Video Tracking

This is a Java 8 / Gradle Wrapper project.
It uses lombok, so remember to enable annotation processing in your IDE.

## Problem
gomo video tracks the users experience of a particular video to a very granular level of
detail. The result is a record for each “fragment” of video watched, detailing at which point in
the video the viewed fragment began and at which point it ended.

One way we try to make sense of this data is the concept of “unique view time” or UVT: that
is, a metric that makes sense of the question, “How much of a given video has this user
watched at least once?”. For example, if I watch the first two minutes of the video and then
go back and rewatch seconds 30 through 45, the UVT is still two minutes. Conversely, if I
watch the first minute and the last minute of a two-hour video, my UVT is also two minutes.

Your objective is to write code that accepts a collection of viewed fragments as input and
outputs the UVT. Viewed Fragments will consist of the start and ending time in ms of a given
watched fragment of video. (It is up to you how to represent viewed fragments in your code.)
Assume that the input is not sorted in any particular way.

### Solution
We have decided to model the fragment entity with a class, which contains the start and end of the watched interval and some methods that are inherent to this entity.

One of this methods is `overlapsWith`, which calculate if the given and passed periods overlaps or collide. This collide logic was a business decision as we think that having the periods `[0,5]` and `[6,9]` is the same than merging them in `[0,9]`, as precision of 1 second is good enough.

To calculate the UVT we have created an utility class which only contains a method to achieve the desired objective.
We are using Java 8 streams to facilitate readability. 
If we go into more details, we can see that the solution is using a temporal `java.util.Stack` to store the computed fragments so we can compute the previos with the current and merge them if they overlap.

### How to run it

Both ways will start a local server listening in the port 8080

#### Executing the JAR

Execute:
```
java -jar build/libs/videotracking-0.0.1-SNAPSHOT.jar
```

#### From IDE

Execute `VideoTrackingApplication.java`

## Documentation
API documentation is included. We are using Spring Rest Docs to auto generate it from the Java Docs and annotations on the entities.
It's under `build/asciidoc/html5` folder. In order to generate it, just run:
```
./gradlew asciidoc
```

**WE NEED TO INVESTIGATE HOW TO AUTOMATICALLY PUBLISH THEM IN BITBUCKET AS AN STATIC WEBSITE (SOMETHING SIMILAR TO GITHUB/GITLAB PAGES)** 

For now we are generating them manually and adding them to the repo, so it can be seen directly on the browser, run the next command and open `docs/index.html` in a browser:
```
./gradlew asciidoc copyDocs
```
