package com.davidr.videotracking.api;

import com.davidr.videotracking.api.model.FragmentDTO;
import com.davidr.videotracking.internal.BaseControllerTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
public class FragmentControllerTests extends BaseControllerTest {

    @Test
    public void shouldReturnTheUVTForOneFragment() throws Exception {
        // given:
        final List<FragmentDTO> requestBody = singletonList(new FragmentDTO(0, 2));

        // expect:
        this.mockMvc.perform(
                post("/fragment")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(requestBody)))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"uvt\": 2}"));
    }

    @Test
    public void shouldReturnBadRequestForInvalidBody() throws Exception {
        // expect:
        this.mockMvc.perform(
                post("/fragment")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"notAValid\": \"requestBody\"}"))
                .andExpect(status().isBadRequest());
    }
}
