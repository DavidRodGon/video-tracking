package com.davidr.videotracking.service

import spock.lang.Specification
import spock.lang.Unroll

class FragmentShould extends Specification {

    @Unroll("#firstFragment should overlap with #secondFragment")
    def 'calculate if a fragment overlaps with other'() {
        expect:
        firstFragment.overlapsWith(secondFragment)

        where:
        firstFragment      | secondFragment
        new Fragment(2, 7) | new Fragment(0, 6)
        new Fragment(0, 6) | new Fragment(2, 7)
        new Fragment(0, 2) | new Fragment(3, 7)
    }
}
