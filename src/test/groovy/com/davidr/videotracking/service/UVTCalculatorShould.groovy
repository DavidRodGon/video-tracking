package com.davidr.videotracking.service

import spock.lang.Specification
import spock.lang.Unroll

class UVTCalculatorShould extends Specification {

    @Unroll("#watchedFragments should return a total of #expectedUVT seconds")
    def 'calculate UVT properly'() {
        expect:
        expectedUVT == UVTCalculator.calculateUVTFor(watchedFragments)

        where:
        expectedUVT || watchedFragments                          | description
        0           || []                                        | 'no fragments'
        5           || [new Fragment(0, 5)]                      | 'only 1 fragment'
        0           || [new Fragment(-1, 5)]                     | 'negative'
        5           || [new Fragment(0, 5), new Fragment(3, 5)]  | 'contained'
        5           || [new Fragment(0, 5), new Fragment(0, 5)]  | 'duplicated'
        8           || [new Fragment(0, 5), new Fragment(7, 10)] | 'separated'
        10          || [new Fragment(0, 5), new Fragment(6, 10)] | 'colliding'
        10          || [new Fragment(6, 10), new Fragment(0, 5)] | 'order changed'
    }
}
