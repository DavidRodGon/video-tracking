package com.davidr.videotracking.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(onConstructor_ = {@JsonCreator})
public class FragmentDTO {

    /*
     * Represents the first second of the watched fragment
     */
    private final int start;

    /*
     * Represents the last second of the watched fragment
     */
    private final int end;
}
