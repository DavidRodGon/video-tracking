package com.davidr.videotracking.api.model;

import lombok.Value;

@Value
public class UvtDTO {

    private final int uvt;
}
