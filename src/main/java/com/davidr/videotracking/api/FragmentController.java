package com.davidr.videotracking.api;

import com.davidr.videotracking.api.model.FragmentDTO;
import com.davidr.videotracking.api.model.UvtDTO;
import com.davidr.videotracking.service.Fragment;
import com.davidr.videotracking.service.UVTCalculator;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/fragment")
public class FragmentController {

    /**
     * Return the UVT given the fragments.
     */
    @PostMapping
    public ResponseEntity<UvtDTO> calculateUVT(@RequestBody final List<FragmentDTO> fragments) {
        final List<Fragment> internalFragments = fragments.stream()
                .map(it -> new Fragment(it.getStart(), it.getEnd()))
                .collect(Collectors.toList());

        final int uvt = UVTCalculator.calculateUVTFor(internalFragments);
        return ResponseEntity.ok(new UvtDTO(uvt));
    }
}
