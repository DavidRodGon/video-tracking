package com.davidr.videotracking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideotrackingApplication {

    public static void main(String[] args) {
        SpringApplication.run(VideotrackingApplication.class, args);
    }

}
