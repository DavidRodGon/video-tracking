package com.davidr.videotracking.service;

import java.util.List;
import java.util.Stack;

import static java.util.Comparator.comparingInt;

public class UVTCalculator {

    public static int calculateUVTFor(final List<Fragment> watchedFragments) {
        return watchedFragments.stream()
                .filter(it -> it.getStart() >= 0 && it.getEnd() >= 0)
                .sorted(comparingInt(Fragment::getStart))
                .reduce(new Stack<Fragment>(),
                        (periods, fragment) -> {
                            if (periods.isEmpty()) {
                                periods.push(fragment);
                            } else {
                                final Fragment lastInsertedFragment = periods.pop();

                                if (lastInsertedFragment.overlapsWith(fragment)) {
                                    final Fragment mergedFragment = new Fragment(Math.min(lastInsertedFragment.getStart(), fragment.getStart()), Math.max(lastInsertedFragment.getEnd(), fragment.getEnd()));
                                    periods.push(mergedFragment);
                                } else {
                                    periods.push(lastInsertedFragment);
                                    periods.push(fragment);
                                }
                            }
                            return periods;
                        },
                        (periods, periods2) -> null)
                .stream()
                .mapToInt(Fragment::getTime)
                .sum();
    }
}
