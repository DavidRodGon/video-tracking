package com.davidr.videotracking.service;

import lombok.Value;

@Value
public class Fragment {

    private final int start;
    private final int end;

    int getTime() {
        return end - start;
    }

    public boolean overlapsWith(final Fragment fragment) {
        return start <= fragment.getStart() && end + 1 >= fragment.getStart()
                || start - 1 <= fragment.getEnd() && end >= fragment.getEnd();
    }
}
